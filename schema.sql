
PRAGMA foreign_keys=1;


CREATE TABLE items(
	id INTEGER PRIMARY KEY,
	name TEXT UNIQUE NOT NULL,
	created_at DATETIME NOT NULL DEFAULT(datetime('now')));

CREATE TABLE cart(
	id INTEGER PRIMARY KEY,
	is_purchased BOOLEAN NOT NULL CHECK(is_purchased IN (0,1)) DEFAULT 0, 
	created_at DATETIME NOT NULL DEFAULT(datetime('now')));

CREATE TABLE users(
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL,
	username TEXT UNIQUE NOT NULL,
	password TEXT NOT NULL,
	token	TEXT,
	cart_id INTEGER NOT NULL,
	FOREIGN KEY (cart_id) REFERENCES cart(id));

CREATE TABLE cartitems(
	cart_id INTEGER NOT NULL,
	item_id INTEGER NOT NULL);

CREATE TABLE orders(
	id INTEGER PRIMARY KEY,
	cart_id INTEGER NOT NULL UNIQUE,
	user_id INTEGER NOT NULL,
	UNIQUE(cart_id, user_id),
	FOREIGN KEY(cart_id) REFERENCES cart(id),
	FOREIGN KEY(user_id) REFERENCES users(id));

CREATE TRIGGER customerIdGenerator AFTER UPDATE OF is_purchased ON cart 
WHEN NEW.is_purchased = 1 
BEGIN
		INSERT INTO orders (cart_id, user_id)VALUES(NEW.id, NEW.user_id);
		INSERT INTO cart DEFAULT VALUES;
		UPDATE users SET 
END;


	
