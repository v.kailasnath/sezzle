package main

import(
	"fmt"
	 "net/http"
	 "github.com/gin-gonic/contrib/static"
	 "github.com/gin-gonic/gin"
	 "gorm.io/gorm"
	 "gorm.io/driver/sqlite"
)



struct user {
	id int,
	name string,
	username string,
	password string,
	token string // To identify the user in the requests
	cart_id ForeignKey(Cart) // One user can have only one cart
	created_at timestamp
}

func main(){
	db, err := gorm.Open(sqlite.Open("shop.db"), &gorm.Config{});
	if err != nil {
		fmt.Println(err);
		panic("failed to connect database");
	}

	// Set the router as the default one shipped with Gin
	router := gin.Default()

	// Serve frontend static files
	router.Use(static.Serve("/", static.LocalFile("./views", true)))

	// Setup route group for the API
	api := router.Group("/api")
	{
		api.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H {
				"message": "pong",
			})
		})
	};
	// Start and run the server

	api.GET("/jokes", JokeHandler);
	api.POST("/jokes/like/:jokeID", LikeJoke);


	api.GET("/user/list",);
	api.GET("/item/list",);
	api.GET("/cart/list",);
	api.GET("/order/list",);
	api.GET("",);
	api.GET("",);

	api.POST("/user/create",);
	api.POST("/user/login",);
	api.POST("/item/create",);
	api.POST("/cart/add",);
	api.POST("/cart/:cartid/complete",);
	api.POST("",);
	router.Run(":3000");
}


func JokeHandler(c *gin.Context){
	c.Header("Content-Type", "application/json");
	c.JSON(http.StatusOK, gin.H {"message": "you need to wait for the joke handler"})
}

func LikeJoke(c *gin.Context){
	c.Header("Content-Type", "application/json");
	c.JSON(http.StatusOK, gin.H {"message": "you need to wait for the joke to be liked"})
}
